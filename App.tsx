import React, { useState, useEffect } from 'react';

import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import styled from 'styled-components/native';
import { GameEngine } from 'react-native-game-engine';

import { Provider } from 'react-redux';
import store from './src/store';

import gameWorld from './src/entities/world';
import gamePhysics from './src/entities/physics';

import useCachedResources from './src/hooks/useCachedResources';

const View = styled.View`
display: flex;
`

const GameEngineStyle = {
  position: "absolute",
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
} as const;

export default function App() {
  const isLoadingComplete = useCachedResources();
  const [isRunning, setIsRunning] = useState(false);

  useEffect(() => {
    setIsRunning((prevState) => !prevState);
  }, []);


  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <Provider store={store} >
          <View>
            <GameEngine
              entities={gameWorld()}
              systems={[gamePhysics]}
              style={GameEngineStyle}
              running={isRunning}
            >
            </GameEngine>
            <StatusBar style="auto" hidden />
          </View>
        </Provider>
      </SafeAreaProvider>
    );
  }
}
