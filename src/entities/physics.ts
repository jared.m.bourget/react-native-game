import Matter from "matter-js";

// Bird: Bird({ world, pos: { x: 50, y: 200 }, size: { height: 40, width: 40 }, color: 'green' }),

type PhysicPropsType = {
    physics: {
        engine: Matter.Engine,
        world: Matter.World,
    };
    Bird: any;
};

type GameTouchType = {
    type: string
}

type GameOptionPropTypes = {
    touches: GameTouchType[];
    time: {
        current: number,
        previous: number | null,
        delta: number,
        previousDelta: number | null,
    };
    dispatch: any;
}

const gamePhysics = (entities: PhysicPropsType, options: GameOptionPropTypes) => {
    const { time, touches } = options;
    const engine = entities.physics.engine;

    touches.filter((touch: GameTouchType)=> touch.type === 'press').forEach((element: any) => {
        Matter.Body.setVelocity(entities.Bird.body, {
            x: 0,
            y: -8
        })
    });

    Matter.Engine.update(engine, time.delta);

    return entities;
};

export default gamePhysics;
