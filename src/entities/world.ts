import Matter from 'matter-js';
import { Bird, Floor } from '../components';

import { Dimensions } from 'react-native';

const gameWorld = () => {
    const engine = Matter.Engine.create({ enableSleeping: false });

    const world = engine.world;

    console.log(engine)
    engine.gravity.y = 0.4;

    const windowHeight = Dimensions.get('window').height;
    const windowWidth = Dimensions.get('window').width;

    console.log({ windowHeight,  windowWidth })

    return {
        physics: { engine, world },
        Bird: Bird({ world, pos: { x: 50, y: 200 }, size: { height: 40, width: 40 }, color: 'green' }),
        Floor: Floor({ world, pos: { x: windowWidth / 2, y: windowHeight }, size: { height: 40, width: windowWidth }, color: 'purple' }),
    }
}

export default gameWorld;
