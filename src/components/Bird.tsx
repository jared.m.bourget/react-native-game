import styled from 'styled-components/native';
import Matter, { World } from 'matter-js';
import React from 'react';
import { Dimensions } from 'react-native';

type BirdPropTypes = {
    world: World,
    color: string,
    pos: {
        x: number,
        y: number,
    },
    size: {
        height: number,
        width: number,
    }
};

const BirdView = styled.View<{ width: number, height: number, xPos: number, yPos: number, color: string }>`
position: absolute;
border-style: solid;
border-width: 2px;
border-color: ${props => props.color};
color: ${props => props.color};
width: ${props => props.width}px;
height: ${props => props.height}px;
left: ${props => props.xPos}px;
top: ${props => props.yPos}px;
`

const Bird = (props: any) => {
    const { color, body } = props;

    /**
     * Y body changes size when switching tabs at the moment
     */
    const bodySize = Math.floor(body.bounds.max.x - body.bounds.min.x);

    const xBody = Math.floor(body.position.x - bodySize / 2 + 200);

    /**
     * 20 is half the floor height at the moment - will probably need to be adjusted
     * 80 is floor height + rectangle height
     */
    let yBody = Math.min(Math.floor((body.position.y - bodySize / 2) - 20), Math.floor(Dimensions.get("window").height - 80));

    return (
        <BirdView yPos={yBody} xPos={xBody} color={color} height={bodySize} width={bodySize} />
    );
};

export default (props: BirdPropTypes) => {
    const { world, color, pos, size } = props;
    console.log(size)

    const initialBird = Matter.Bodies.rectangle(
        pos.x,
        pos.y,
        size.width,
        size.height,
        { label: "Bird" }
    );

    Matter.World.add(world, initialBird);

    return {
        body: initialBird,
        color,
        pos,
        renderer: <Bird />
    };
};

