import styled from 'styled-components/native';
import Matter, { World } from 'matter-js';
import React from 'react';
import { Dimensions } from 'react-native';

type FloorPropTypes = {
    world: World,
    color: string,
    pos: {
        x: number,
        y: number,
    },
    size: {
        height: number,
        width: number,
    }
};

const FloorView = styled.View<{ width: number, height: number, xPos: number, yPos: number, color: string }>`
position: absolute;
background-color: ${props => props.color};
color: ${props => props.color};
width: ${props => props.width}px;
height: ${props => props.height}px;
left: ${props => props.xPos}px;
top: ${props => props.yPos}px;
`

const Floor = (props: any) => {
    const { color, body } = props;

    const widthBody = body.bounds.max.x;
    const heightBody = Math.floor(body.bounds.max.y - body.bounds.min.y);

    const xBody = Math.floor(body.position.x- widthBody / 2);
    // const yBody = Math.floor(body.position.y - heightBody);
    const yBody = Math.floor(Dimensions.get("window").height - heightBody);
    // const yBody = Math.max(Math.floor(body.position.y - heightBody), Math.floor(Dimensions.get("window").height - heightBody));

    return (
        <FloorView yPos={yBody} xPos={xBody} color={color} height={heightBody} width={widthBody} />
    );
};

export default (props: FloorPropTypes) => {
    const { world, color, pos, size } = props;

    const floor = Matter.Bodies.rectangle(
        pos.x,
        pos.y,
        size.width,
        size.height,
        {
            label: "Floor",
            isStatic: true,
        }
    );

    Matter.World.add(world, floor);

    return {
        body: floor,
        color,
        pos,
        renderer: <Floor />
    };
};

