import styled from 'styled-components/native'
import { Text } from 'react-native';
import { RootTabScreenProps } from '../types';

const TabOneView = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: #eee;
`

const Title = styled.Text`
font-size: 20em;
font-weight: bold;
`

const Divider = styled.View<{lightColor?: boolean}>`
margin-top: 30px;
margin-bottom: 30px;
height: 2px;
width: 80%;
background-color: ${props => props.lightColor ? '#000' : 'rgba(255,255,255,0.1)'}
`

export default function TabOneScreen({ navigation }: RootTabScreenProps<'TabOne'>) {
  return (
    <TabOneView>
      <Title>Tab One</Title>
      <Text>Some update to screen</Text>
      <Divider lightColor />
    </TabOneView>
  );
}
