const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';

export default {
  primary: {
    '500': '#251D35',
    '600': '#1A1423',
    '700': '#130F1A',
    '800': '#0A070D',
  },
  secondary: {
    '500': '#B75D69',
    '600': '#A94C58',
    '700': '#8D3F4A',
    '800': '#71333B',
  },
  divider: {
    '500': '#F9F3F0',
    '600': '#F4E7E1',
    '700': '#EFDAD1',
    '800': '#EACDC2',
  },
  light: {
    text: '#000',
    background: '#fff',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
  },
  dark: {
    text: '#fff',
    background: '#000',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
  },
};
