import Colors from "./Colors";

const Theme = {
  ...Colors,
  fonts: ['Roboto', 'sans-serif'],
  fontSizes: {
    xs: '0.8rem',
    s: '1.0rem',
    md: '1.5rem',
    lg: '3rem',
    xl: '4rem',
  },
  fontWeights: {
    light: 300,
    normal: 500,
    bold: 700,
  }
}

export default Theme;