import AppState from '../types';
import Theme from '../../constants/Theme';

type PayloadAction = {
  type: string,
  theme: string,
};

const INITIAL_STATE = {
  theme: { ...Theme.dark }
};

const themeReducer = (state: AppState, action: PayloadAction) => {
  switch (action.type) {
    case 'theme/SWITCH_THEME':
      return {
        ...state,
        theme: { ...state.theme }
      }
    default: return {
      ...INITIAL_STATE
    }
  }
};

export default themeReducer;

