import { combineReducers } from '@reduxjs/toolkit';
import themeReducer from './themeReducer';

const rootReducer = combineReducers({
  themeReducer
});

export default rootReducer;
