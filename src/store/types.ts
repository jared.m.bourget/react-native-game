type AppState = {
  theme: { [x: string]: string }, 
}

export default AppState;
