
const SWITCH_THEME = 'theme/SWITCH_THEME';

const switchTheme = (theme: string) => {
  return {
    type: SWITCH_THEME,
    payload: theme,
  };
};

export default switchTheme;
